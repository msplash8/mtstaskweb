import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { UserProfileService } from "../user-profile.service";
import { UserProfile } from "../userprofile";

@Component({
  selector: 'app-user-profile-info',
  templateUrl: './user-profile-info.component.html',
  styleUrls: ['./user-profile-info.component.css']
})
export class UserProfileInfoComponent implements OnInit {

  profile: UserProfile;
  sub: any;
  constructor(private route: ActivatedRoute,
    private profileService: UserProfileService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let login = params['login'];
      console.log('login: ' + login);
      this.profileService.get(login).subscribe(u => this.profile = u);
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
