import { Component, OnInit } from '@angular/core';
import { UserProfile } from '../userprofile';
import { UserProfileService } from '../user-profile.service';

@Component({
  selector: 'app-user-profile-list',
  templateUrl: './user-profile-list.component.html',
  styleUrls: ['./user-profile-list.component.css']
})
export class UserProfileListComponent implements OnInit {
  profiles: UserProfile[] = [];
  constructor(private profileService: UserProfileService) {

  }

  ngOnInit() {
    this.profileService.getAll().subscribe(u => {
      this.profiles = u; console.log(u);
    });

  }

}
