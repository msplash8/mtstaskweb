import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { UserProfile } from './userprofile';

@Injectable()
export class UserProfileService {
  private baseUrl: string = 'http://localhost:8080/task/user-profiles';
  constructor(private http: HttpClient) { }
  getAll(): Observable<UserProfile[]> {
    let users = this.http
      .get<UserProfile[]>(`${this.baseUrl}`, { headers: this.getHeaders() });
    return users;
  }

  get(login: string): Observable<UserProfile> {
    let profile$ = this.http
      .get(`${this.baseUrl}/${login}`, { headers: this.getHeaders() });
    return profile$;
  }

  private getHeaders() {
    let headers = new HttpHeaders().set('Accept', 'application/json');
    headers.append('Accept', 'application/json');
    return headers;
  }

}
