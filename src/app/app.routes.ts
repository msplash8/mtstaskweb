import { Routes, RouterModule } from '@angular/router';
import { UserProfileListComponent } from './user-profile-list/user-profile-list.component';
import { UserProfileInfoComponent } from './user-profile-info/user-profile-info.component';
const routes: Routes = [
  {
    path: 'user-profiles',
    component: UserProfileListComponent
  },
  {
    path: 'user-profiles/:login',
    component: UserProfileInfoComponent
  }
];

export const appRouterModule = RouterModule.forRoot(routes);
