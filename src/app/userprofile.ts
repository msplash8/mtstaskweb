export interface UserProfile {
  displayName: string;
  login: string;
  password: string;
  birthDate: number;
  gender: number;
}
