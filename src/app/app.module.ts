import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { appRouterModule } from './app.routes';
import { UserProfileService } from './user-profile.service';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { UserProfileListComponent } from './user-profile-list/user-profile-list.component';
import { UserProfileInfoComponent } from './user-profile-info/user-profile-info.component';

@NgModule({
  declarations: [
    AppComponent,
    UserProfileListComponent,
    UserProfileInfoComponent
  ],
  imports: [
    BrowserModule,
    appRouterModule,
    HttpClientModule
  ],
  providers: [UserProfileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
